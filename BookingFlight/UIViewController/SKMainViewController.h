//
//  SKMainViewController.h
//  Кавай
//
//  Created by niko on 28.01.17.
//  Copyright © 2017 niko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SKMainViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *sumaLabel;
@property (weak, nonatomic) IBOutlet UILabel *sumaCurrencyLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end
