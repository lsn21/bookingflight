//
//  AppDelegate.h
//  Кавай
//
//  Created by niko on 28.01.17.
//  Copyright © 2017 niko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

